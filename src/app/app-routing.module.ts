import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent} from "./core/layout/layout.component";
import {AboutComponent} from "./pages/about/about.component";

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      { path: 'about', pathMatch: 'full', component: AboutComponent},
      {
        path: 'musicnumbers',
        loadChildren: () =>
          import('./pages/musicnumber/musicnumber.module').then((m) => m.MusicnumberModule)
      },
      {
        path: 'artists',
        loadChildren: () =>
          import('./pages/artist/artist.module').then((m) => m.ArtistModule)
      },
      {
        path: 'bands',
        loadChildren: () =>
          import('./pages/band/band.module').then((m) => m.BandModule)
      },
    ]
  },
  { path: '**', redirectTo: '/'}
]

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
