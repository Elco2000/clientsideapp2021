import * as fromComponents from '.';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {MatTableModule} from "@angular/material/table";
import {MatCardModule} from "@angular/material/card";
import {MatGridListModule} from "@angular/material/grid-list";
import {MatListModule} from "@angular/material/list";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatSelectModule} from "@angular/material/select";
import {MatInputModule} from "@angular/material/input";
import {MatDatepickerModule,} from "@angular/material/datepicker";
import {MatButtonModule} from "@angular/material/button";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatIconModule} from "@angular/material/icon";
import {MatNativeDateModule} from "@angular/material/core";
import {MatMenuModule} from "@angular/material/menu";
import {MatPaginatorModule} from "@angular/material/paginator";


const routes: Routes = [
  {path: '', pathMatch: 'full', component: fromComponents.ArtistlistComponent},
  {path: 'create', pathMatch: 'full', component: fromComponents.ArtistcreateComponent},
  {path: 'edit/:id', pathMatch: 'full', component: fromComponents.ArtisteditComponent},
  {path: ':id', pathMatch: 'full', component: fromComponents.ArtistbyidComponent},
]

@NgModule({
  declarations: [...fromComponents.components],
  imports: [RouterModule.forChild(routes), CommonModule, MatTableModule, MatCardModule, MatGridListModule,
    MatListModule, MatFormFieldModule, MatSelectModule, MatInputModule, MatDatepickerModule, MatButtonModule,
    FormsModule, ReactiveFormsModule, MatIconModule, MatNativeDateModule, MatMenuModule, MatPaginatorModule]
})

export class ArtistModule {}
