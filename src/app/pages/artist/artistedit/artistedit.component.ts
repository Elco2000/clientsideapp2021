import { Component, OnInit } from '@angular/core';
import {Artist} from "../artist.model";
import {ArtistService} from "../artist.service";
import {ActivatedRoute} from "@angular/router";
import {Location} from "@angular/common";

@Component({
  selector: 'app-artistedit',
  templateUrl: './artistedit.component.html',
  styleUrls: ['./artistedit.component.scss']
})
export class ArtisteditComponent implements OnInit {
  artist!: Artist;

  constructor(private artistService: ArtistService, private route: ActivatedRoute, private location: Location) { }

  ngOnInit(): void {
    this.getArtist()
  }

  getArtist(): void {
    const id = this.route.snapshot.paramMap.get('id')
    this.artistService.getArtistById(id)
      .subscribe(artist => this.artist = artist)
  }

  save(): void {
    this.artistService.updateArtist(this.artist)
      .subscribe(artist => {
        this.goBack()
      })
  }

  goBack(): void {
    this.location.back();
  }
}
