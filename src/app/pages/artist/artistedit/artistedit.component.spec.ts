import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ArtisteditComponent} from './artistedit.component';
import {Artist} from "../artist.model";
import {Directive, HostListener, Input, Component} from "@angular/core";
import {ArtistService} from "../artist.service";
import {ActivatedRoute, convertToParamMap, Router} from "@angular/router";
import {Observable, of, Subscription, BehaviorSubject} from "rxjs";
import {MatCardModule} from "@angular/material/card";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

@Directive({
  // tslint:disable-next-line: directive-selector
  selector: '[routerLink]',
})
export class RouterLinkStubDirective {
  @Input('routerLink') linkParams: any;
  navigatedTo: any = null;

  @HostListener('click')
  onClick(): void {
    this.navigatedTo = this.linkParams;
  }
}

// Mock of objects
const oneArtist: Artist = {
  _id: '1',
  firstname: 'Piet',
  lastname: 'Doe',
  nickname: 'Example Artist 2',
  birthdate: new Date("Fri Dec 08 2019 07:44:57"),
  nationality: 'Testland2',
  stillActive: false
}

describe('ArtisteditComponent', () => {
  let component: ArtisteditComponent;
  let fixture: ComponentFixture<ArtisteditComponent>;

  let artistServiceSpy: any;
  let activatedRouteSpy;
  let locationSpy;

  beforeEach(() => {
    artistServiceSpy = jasmine.createSpyObj('ArtistService', ['getArtistById', 'updateArtist', 'getArtists'])
    // routeSpy = jasmine.createSpyObj('ActivatedRoute', ['snapshot']);
    activatedRouteSpy = {
      snapshot: {
        paramMap: convertToParamMap({
          id: '1',
        })
      }
    };


    TestBed.configureTestingModule({
      declarations: [
        ArtisteditComponent,
        RouterLinkStubDirective
      ],
      imports: [MatCardModule, FormsModule, ReactiveFormsModule],
      providers: [
        {provide: ArtistService, useValue: artistServiceSpy},
        {provide: ActivatedRoute, useValue: activatedRouteSpy},
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(ArtisteditComponent);
    component = fixture.componentInstance;
  });

  afterEach(() => {
    fixture.destroy();
  })

  fit('should create', (done) => {
    artistServiceSpy.getArtistById.and.returnValue(of(oneArtist));

    fixture.detectChanges();
    expect(component).toBeTruthy();

    setTimeout(() => {
      expect(component.artist).toEqual(oneArtist);
      done();
    }, 200);
  });
});
