import {ArtistlistComponent} from "./artistlist/artistlist.component";
import {ArtistbyidComponent} from "./artistbyid/artistbyid.component";
import {ArtistcreateComponent} from "./artistcreate/artistcreate.component";
import {ArtisteditComponent} from "./artistedit/artistedit.component";

export const components: any[] = [
  ArtistlistComponent,
  ArtistbyidComponent,
  ArtistcreateComponent,
  ArtisteditComponent
]

export * from './artistlist/artistlist.component';
export * from './artistbyid/artistbyid.component';
export * from './artistcreate/artistcreate.component';
export * from './artistedit/artistedit.component';
