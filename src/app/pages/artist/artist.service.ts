import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";

import {map, Observable} from "rxjs";
import {Artist} from "./artist.model";


@Injectable({
  providedIn: 'root'
})
export class ArtistService {

  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  //TO-DO move it to the ENV file.
  private urlApi = "https://clientsideapi2021.herokuapp.com/api/artists"

  constructor(private http: HttpClient) {
  }

  getArtists(): Observable<Artist[]> {
    return this.http.get<Artist[]>(this.urlApi);
  }

  getArtistById(id: string | null): Observable<Artist> {
    return this.http.get<Artist>(this.urlApi + "/" + id);
  }

  addArtist(artist: Artist): Observable<Artist> {
    return this.http.post<Artist>(this.urlApi, artist, this.httpOptions)
  }

  updateArtist(artist: Artist): Observable<any> {
    return this.http.put(this.urlApi + '/' + artist._id, artist, this.httpOptions);
  }

  deleteArtist(id: string): Observable<Artist> {
    return this.http.delete<Artist>(this.urlApi + '/' + id);
  }
}
