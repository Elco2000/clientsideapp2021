import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormControl, FormGroup, Validators} from "@angular/forms";
import {Artist} from "../artist.model";
import {ArtistService} from "../artist.service";
import {Location} from "@angular/common";

@Component({
  selector: 'app-artistcreate',
  templateUrl: './artistcreate.component.html',
  styleUrls: ['./artistcreate.component.scss']
})
export class ArtistcreateComponent implements OnInit {
  artistForm!: FormGroup;
  artists!: Artist[];


  constructor(private artistService: ArtistService, private location: Location) { }

  ngOnInit(): void {
    this.artistForm = new FormGroup({
      firstname: new FormControl('', [
        Validators.required,
        Validators.minLength(2)
      ]),
      lastname: new FormControl('',[
        Validators.required,
        Validators.minLength(4)
      ]),
      nickname: new FormControl('', [
        Validators.required,
        Validators.minLength(4)
      ]),
      birthdate: new FormControl('', [
        Validators.required
      ]),
      nationality: new FormControl('', [
        Validators.required,
        Validators.minLength(4)
      ]),
      stillActive: new FormControl('', [
        Validators.required,
      ])
    })

    this.getArtists();
  }

  getFirstname() { return this.artistForm.get('firstname'); }
  getLastname() { return this.artistForm.get('lastname'); }
  getNickname() { return this.artistForm.get('nickname'); }
  getBirthdate() { return this.artistForm.get('birthday'); }
  getNationality() { return this.artistForm.get('nationality'); }
  getStillActive() { return this.artistForm.get('stillActive'); }

  // // @ts-ignore
  // getErrorMessage(element: AbstractControl) {
  //   if(element.hasError('required')) {
  //     return('You must enter a value');
  //   } else if (element.hasError('minlength')) {
  //     return('The value must have a min length of ' + length);
  //   }
  // }

  getArtists(): void {
    this.artistService.getArtists()
      .subscribe(artists => this.artists = artists)
  }

  add(firstname: string, lastname: string, nickname: string, birthdateAsString: string,
      nationality: string, stillActiveString: string): void {
    const birthdate = new Date(birthdateAsString)
    birthdate.setMinutes( birthdate.getMinutes() + 480 );
    let stillActive = true;

    if (stillActiveString === 'false') {
      stillActive = false;
    }

    if(!this.artistForm.valid){
      return;
    } else {
      this.artistService.addArtist({ firstname, lastname, nickname, birthdate, nationality, stillActive } as Artist)
        .subscribe(artist => {
          this.artists.push(artist);
          this.goBack();
        });
    }
  }

  goBack(): void {
    this.location.back();
  }



}
