export class Artist {

  constructor(
    public _id: string,
    public firstname: string,
    public lastname: string,
    public nickname: string,
    public birthdate: Date,
    public nationality: string,
    public stillActive: boolean
  ) {
  }
}
