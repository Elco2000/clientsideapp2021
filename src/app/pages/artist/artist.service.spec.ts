import {HttpClient, HttpResponse} from "@angular/common/http";
import {TestBed} from '@angular/core/testing';
import {of} from 'rxjs';
import {Artist} from "./artist.model";
import {ArtistService} from './artist.service';

// Mock of Objects
const expectedArtists: Artist[] = [
  {
    _id: 'mongo_id',
    firstname: 'John',
    lastname: 'Doe',
    nickname: 'Example Artist',
    birthdate: new Date("Fri Dec 08 2019 07:44:57"),
    nationality: 'Testland',
    stillActive: true
  }
]

const oneArtist: Artist = {
  _id: 'mongo_id',
  firstname: 'Piet',
  lastname: 'Doe',
  nickname: 'Example Artist 2',
  birthdate: new Date("Fri Dec 08 2019 07:44:57"),
  nationality: 'Testland2',
  stillActive: false
}


describe('ArtistService', () => {
  let service: ArtistService;
  let httpSpy: jasmine.SpyObj<HttpClient>

  beforeEach(() => {
    httpSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);

    TestBed.configureTestingModule({
      providers: [{provide: HttpClient, useValue: httpSpy}]
    });
    service = TestBed.inject(ArtistService);
    httpSpy = TestBed.inject(HttpClient) as jasmine.SpyObj<HttpClient>;
  });

  fit('should be created', () => {
    expect(service).toBeTruthy();
  });

  // Test for GetArtists
  fit('should return a list of artists', (done: DoneFn) => {
    httpSpy.get.and.returnValue(of(expectedArtists));

   service.getArtists().subscribe((artists: Artist[]) => {
      console.log(artists);
      expect(artists.length).toBe(1);
      expect(artists[0]._id).toEqual(expectedArtists[0]._id);
      done();
    });
  });

  // Test for GetArtistById
  fit('should return one specific artist', (done: DoneFn) => {
    httpSpy.get.and.returnValue(of(oneArtist));
    const _id = 'mongo_id';

    service.getArtistById(_id).subscribe((artist: Artist) => {
      console.log(artist);
      expect(artist._id).toEqual(oneArtist._id);
      done();
    });
  });

  // Test for addArtist
  fit('should add an artist and return it at the end', (done: DoneFn) => {
    httpSpy.post.and.returnValue(of(oneArtist));

    service.addArtist(oneArtist).subscribe((artist: Artist) => {
      console.log(artist);
      expect(artist._id).toEqual(oneArtist._id);
      done();
    });
  });

  // Test for updateArtist
  fit('should update an artist and return it at the end', (done: DoneFn) => {
    const updateArtist = oneArtist;
    updateArtist.firstname = "Frits";

    httpSpy.put.and.returnValue(of(updateArtist));

    service.updateArtist(updateArtist).subscribe((artist: Artist) => {
      console.log(updateArtist);
      expect(artist.firstname).toEqual("Frits");
      done();
    });
  });

  // Test for deleteArtist
  fit('should delete an artist and return a 200 response', (done: DoneFn) => {
    httpSpy.delete.and.returnValue(of(oneArtist))

    service.deleteArtist(oneArtist._id).subscribe((artist: Artist) =>{
      console.log(oneArtist);
      expect(artist._id).toEqual(oneArtist._id);
      done();
    });
  });

});
