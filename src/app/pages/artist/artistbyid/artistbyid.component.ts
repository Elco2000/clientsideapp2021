import { Component, OnInit } from '@angular/core';
import {Artist} from "../artist.model";
import {ArtistService} from "../artist.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-artistbyid',
  templateUrl: './artistbyid.component.html',
  styleUrls: ['./artistbyid.component.scss']
})
export class ArtistbyidComponent implements OnInit {
  artist!: Artist;

  constructor(private artistService: ArtistService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.getArtist();
  }

  getArtist(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.artistService.getArtistById(id)
      .subscribe(artist => this.artist = artist);
  }

}
