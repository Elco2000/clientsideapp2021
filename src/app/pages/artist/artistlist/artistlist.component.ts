import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {ArtistService} from "../artist.service";
import {Artist} from "../artist.model";
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";

@Component({
  selector: 'app-artistlist',
  templateUrl: './artistlist.component.html',
  styleUrls: ['./artistlist.component.scss']
})
export class ArtistlistComponent implements OnInit, AfterViewInit {
  displayedColumns: string[] = ['firstname', 'lastname', 'nickname', 'birthdate', 'nationality', 'stillActive', 'options']
  artists!: Artist[];
  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(private artistService: ArtistService) {
  }

  ngOnInit(): void {
    this.getArtist();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  getArtist(): void {
    this.artistService.getArtists()
      .subscribe(artists => {
        this.artists = artists;
        this.dataSource.data = this.artists;
      })
  }

  delete(id: string): void {
    this.artistService.deleteArtist(id).subscribe((artist) => {
      this.ngOnInit();
    })
  }
}
