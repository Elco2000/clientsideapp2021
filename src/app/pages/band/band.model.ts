import {BandArtist} from "./bandArtist.model";

export class Band {

  constructor(
    public _id: string,
    public name: string,
    public stillActive: boolean,
    public artists: any[]
  ) {
  }
}
