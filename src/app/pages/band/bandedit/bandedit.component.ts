import {Component, OnInit} from '@angular/core';
import {Band} from "../band.model";
import {Artist} from "../../artist/artist.model";
import {BandService} from "../band.service";
import {ArtistService} from "../../artist/artist.service";
import {ActivatedRoute} from "@angular/router";
import {Location} from "@angular/common";
import {BandArtist} from "../bandArtist.model";
import {FormControl} from "@angular/forms";

@Component({
  selector: 'app-bandedit',
  templateUrl: './bandedit.component.html',
  styleUrls: ['./bandedit.component.scss']
})
export class BandeditComponent implements OnInit {
  band!: Band;
  artists!: Artist[];

  constructor(private bandService: BandService, private artistService: ArtistService, private route: ActivatedRoute, private location: Location) {
  }

  ngOnInit(): void {
    this.getBand();
    this.getArtists();
  }

  getBand(): void {
    const id = this.route.snapshot.paramMap.get('id')
    this.bandService.getBandById(id)
      .subscribe(band => this.band = band)
  }

  getArtists(): void {
    this.artistService.getArtists()
      .subscribe(artists => this.artists = artists);
  }

  save(): void {
    this.bandService.updateBand(this.band)
      .subscribe(band => {
        this.goBack()
      })
  }

  goBack(): void {
    this.location.back();
  }

}
