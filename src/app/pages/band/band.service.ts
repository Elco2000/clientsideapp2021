import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {Band} from "./band.model";

@Injectable({
  providedIn: 'root'
})
export class BandService {

  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  private urlApi = "https://clientsideapi2021.herokuapp.com/api/bands"

  constructor(private http: HttpClient) {
  }

  getBands(): Observable<Band[]> {
    return this.http.get<Band[]>(this.urlApi)
  }

  getBandById(id: string | null): Observable<Band> {
    return this.http.get<Band>(this.urlApi + "/" + id);
  }

  addBand(band: Band): Observable<Band> {
    return this.http.post<Band>(this.urlApi, band, this.httpOptions);
  }

  updateBand(band: Band): Observable<any> {
    return this.http.put(this.urlApi + '/' + band._id, band);
  }

  deleteBand(id: string): Observable<Band> {
    return this.http.delete<Band>(this.urlApi + '/' + id);
  }
}
