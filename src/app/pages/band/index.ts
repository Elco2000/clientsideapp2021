import {BandlistComponent} from "./bandlist/bandlist.component";
import {BandbyidComponent} from "./bandbyid/bandbyid.component";
import {BandcreateComponent} from "./bandcreate/bandcreate.component";
import {BandeditComponent} from "./bandedit/bandedit.component";

export const components: any[] = [
  BandlistComponent,
  BandbyidComponent,
  BandcreateComponent,
  BandeditComponent
]

export * from './bandlist/bandlist.component';
export * from './bandbyid/bandbyid.component';
export * from './bandcreate/bandcreate.component';
export * from './bandedit/bandedit.component';
