import { Component, OnInit } from '@angular/core';
import {FormControl, FormControlName, FormGroup, Validators} from "@angular/forms";
import {Band} from "../band.model";
import {BandService} from "../band.service";
import {ArtistService} from "../../artist/artist.service";
import {Location} from "@angular/common";
import {Artist} from "../../artist/artist.model";

@Component({
  selector: 'app-bandcreate',
  templateUrl: './bandcreate.component.html',
  styleUrls: ['./bandcreate.component.scss']
})
export class BandcreateComponent implements OnInit {
  bandForm!: FormGroup;
  bands!: Band[];
  artists!: Artist[];

  constructor(private bandService: BandService, private artistService: ArtistService, private location: Location) { }

  ngOnInit(): void {
    this.bandForm = new FormGroup({
      name: new FormControl('', [
        Validators.required,
        Validators.minLength(2)
      ]),
      stillActive: new FormControl('', [
        Validators.required
      ]),
      artistsForm: new FormControl('', [
        Validators.required
      ])
    })

    this.getBands();
    this.getArtists();
  }

  getName() { return this.bandForm.get('name');}
  getStillActive() { return this.bandForm.get('stillActive');}
  getArtistsForm() { return this.bandForm.get('artistsForm');}

  getBands(): void {
    this.bandService.getBands()
      .subscribe(bands => this.bands = bands);
  }

  getArtists(): void {
    this.artistService.getArtists()
      .subscribe(artists => this.artists = artists);
  }

  add(name: string, stillActive: boolean, artists: string[]): void {
    if(!this.bandForm.valid) {
      return;
    } else {
      this.bandService.addBand({ name, stillActive, artists }  as Band)
        .subscribe(band => {
          this.bands.push(band);
          this.goBack();
        })
    }
  }

  goBack(): void {
    this.location.back();
  }

}
