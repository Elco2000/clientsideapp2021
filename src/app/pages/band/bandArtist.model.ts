export class BandArtist {

  constructor(
    public _id: string,
    public firstname: string,
    public lastname: string,
    public nickname: string
  ) {
  }
}
