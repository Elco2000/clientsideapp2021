import * as fromComponents from '.';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {from} from "rxjs";
import {MatCardModule} from "@angular/material/card";
import {MatTableModule} from "@angular/material/table";
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";
import {MatMenuModule} from "@angular/material/menu";
import {MatListModule} from "@angular/material/list";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatSelectModule} from "@angular/material/select";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatGridListModule} from "@angular/material/grid-list";


const routes: Routes = [
  {path: '', pathMatch: 'full', component: fromComponents.BandlistComponent},
  {path: 'create', pathMatch: 'full', component: fromComponents.BandcreateComponent},
  {path: 'edit/:id', pathMatch: 'full', component: fromComponents.BandeditComponent},
  {path: ':id', pathMatch: 'full', component: fromComponents.BandbyidComponent}
]

@NgModule({
    declarations: [...fromComponents.components],
  imports: [RouterModule.forChild(routes), CommonModule, MatCardModule, MatTableModule, MatButtonModule, MatIconModule,
    MatMenuModule, MatListModule, MatFormFieldModule, MatInputModule, MatSelectModule, FormsModule, ReactiveFormsModule, MatPaginatorModule, MatGridListModule]
})

export class BandModule {
}
