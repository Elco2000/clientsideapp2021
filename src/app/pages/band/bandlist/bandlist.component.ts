import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {Band} from "../band.model";
import {BandService} from "../band.service";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {MatTableDataSource} from "@angular/material/table";

@Component({
  selector: 'app-bandlist',
  templateUrl: './bandlist.component.html',
  styleUrls: ['./bandlist.component.scss']
})
export class BandlistComponent implements OnInit, AfterViewInit {
  displayedColumns: string[] = ['name', 'stillActive', 'options'];
  bands!: Band[];
  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(private bandService: BandService) {
  }

  ngOnInit(): void {
    this.getBands();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }


  getBands(): void {
    this.bandService.getBands()
      .subscribe(bands => {
        this.bands = bands;
        this.dataSource.data = this.bands;
      })
  }

  delete(id: string): void {
    this.bandService.deleteBand(id).subscribe((band) => {
      this.ngOnInit();
    })
  }
}
