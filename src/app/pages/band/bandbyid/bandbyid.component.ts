import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {Band} from "../band.model";
import {BandService} from "../band.service";
import {ActivatedRoute} from "@angular/router";
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";

@Component({
  selector: 'app-bandbyid',
  templateUrl: './bandbyid.component.html',
  styleUrls: ['./bandbyid.component.scss']
})
export class BandbyidComponent implements OnInit {
  band!: Band;

  constructor(private bandService: BandService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.getBand();
  }

  getBand(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.bandService.getBandById(id)
      .subscribe(band => {
        this.band = band;
        console.log(this.band);
      });
  }

}
