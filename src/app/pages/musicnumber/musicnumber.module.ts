import * as fromComponents from '.';
import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";

const routes: Routes = [
  {path: '', pathMatch: 'full', component: fromComponents.MusicnumberlistComponent}
]

@NgModule({
  declarations: [...fromComponents.components],
  imports: [RouterModule.forChild(routes)]
})

export class MusicnumberModule {}
