import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutComponent } from './core/layout/layout.component';
import { NavComponent } from './core/nav/nav.component';
import {MatTabsModule} from "@angular/material/tabs";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatButtonModule} from "@angular/material/button";

import {HttpClientModule} from "@angular/common/http";
import { AboutComponent } from './pages/about/about.component';
import {MatCardModule} from "@angular/material/card";
import {MatGridListModule} from "@angular/material/grid-list";
// import {RouterLinkStubDirective} from "./pages/artist/artistedit/artistedit.component.spec";
// import {RouterLinkStubDirective} from "./pages/artist/artistedit/artistedit.component.spec";

@NgModule({
    declarations: [
        AppComponent,
        LayoutComponent,
        NavComponent,
        AboutComponent,
    ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatTabsModule,
    MatToolbarModule,
    MatButtonModule,
    HttpClientModule,
    MatCardModule,
    MatGridListModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
